# Global constants
REGISTRY_NAME                   := registry.gitlab.com/mj-docker-utils
GIT_COMMIT_HASH                 := $$(git log -1 --pretty=%H)

# Container constants
CONTAINER_IMAGE_NAME_BASE		:= alpine-bats-test-runner
CONTAINER_FULL_IMAGE_NAME       := ${REGISTRY_NAME}/${CONTAINER_IMAGE_NAME_BASE}

CONTAINER_FULL_IMAGE_HASH       := ${CONTAINER_FULL_IMAGE_NAME}:${GIT_COMMIT_HASH}
CONTAINER_FULL_IMAGE_TESTING    := ${CONTAINER_FULL_IMAGE_NAME}:testing
CONTAINER_FULL_IMAGE_LATEST     := ${CONTAINER_FULL_IMAGE_NAME}:latest

CONTAINER_NAME                  := ${CONTAINER_IMAGE_NAME_BASE}
CONTAINER_NAME_TEST             := test_${CONTAINER_IMAGE_NAME_BASE}
 

### Container building and testing ###

build:
	docker build -t ${CONTAINER_FULL_IMAGE_HASH} .
	docker tag ${CONTAINER_FULL_IMAGE_HASH} ${CONTAINER_FULL_IMAGE_TESTING}
	docker push ${CONTAINER_FULL_IMAGE_HASH}
	docker push ${CONTAINER_FULL_IMAGE_TESTING}

system_tests:
	cd system_tests/ && ./system_tests.sh

tag_latest:
	# tag the latest testing binaries as latest
	docker tag ${CONTAINER_FULL_IMAGE_TESTING} ${CONTAINER_FULL_IMAGE_LATEST}
	docker push ${CONTAINER_FULL_IMAGE_LATEST}

.PHONY:	system_tests