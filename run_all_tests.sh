#!/bin/bash

# run all the Bats tests from the /tests directory
bats -t /tests/*.bats
BATS_RESULT=$?

# return the test result
exit $BATS_RESULT
