[![pipeline status](https://gitlab.com/mj-docker-utils/alpine-bats-test-runner/badges/master/pipeline.svg)](https://gitlab.com/mj-docker-utils/alpine-bats-test-runner/commits/master)

# Alpine-bats-test-runner - Alpine based test runner image for the automated testing with Bats: Bash Automated Testing System. Includes also Dockerize for dependency waiting.

Image that could help to reduce some repetitive work for those who run Bats tests from the docker. Based on the alpine linux and easily enhanceable.

## Technical description

The container uses runs a scipt from the CMD that calls Bats to run all the test files from the /tests directory. The output of the tests is then used as an output of the container run.

## Usage

### Run instance of registry.gitlab.com/mj-docker-utils/alpine-bats-test-runner:latest with your tests mapped using volume

The simpliest usage is to use the latest version of the built image and map the directory with your tests into the container via the command line arguments.

	docker run                                                                    \
	  --rm                                                                        \
	  -v ${PWD}/my_tests:/tests                                                   \
	  --name my_test_runner                                                       \
	  registry.gitlab.com/mj-docker-utils/alpine-bats-test-runner:latest
