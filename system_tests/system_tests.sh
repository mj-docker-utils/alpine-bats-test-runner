#!/bin/bash

function Should_succeed_if_all_tests_from_dir_mapped_as_volume_succeed {

    # GIVEN

    # WHEN

	# Run the Bats tests in the container and map the ./tests
	# and map a volume into the container to provide custom cron job script and receive the check the output.
	docker run                                                                    \
	  --rm                                                                        \
	  -v ${PWD}/successful_tests:/tests                                           \
	  --name test_should_run_all_tests_from_tests_dir_mapped_as_volume            \
	  registry.gitlab.com/mj-docker-utils/alpine-bats-test-runner:testing
	RES=$?

    # THEN
	if [ "$RES" = "0" ]; then 
		echo "OK - Should_succeed_if_all_tests_from_dir_mapped_as_volume_succeed"
	else
		echo "not OK - Should_succeed_if_all_tests_from_dir_mapped_as_volume_succeed"
		return 1
	fi

	return 0
}

function Should_fail_if_one_test_from_dir_mapped_as_volume_fails {

    # GIVEN

    # WHEN

	# Run the Bats tests in the container and map the ./tests
	# and map a volume into the container to provide custom cron job script and receive the check the output.
	docker run                                                                    \
	  --rm                                                                        \
	  -v ${PWD}/single_failing_test:/tests                                        \
	  --name test_should_run_all_tests_from_tests_dir_mapped_as_volume            \
	  registry.gitlab.com/mj-docker-utils/alpine-bats-test-runner:testing
	RES=$?

    # THEN
	if [ "$RES" != "0" ]; then 
		echo "OK - Should_fail_if_one_test_from_dir_mapped_as_volume_fails"
	else
		echo "not OK - Should_fail_if_one_test_from_dir_mapped_as_volume_fails"
		return 1
	fi

	return 0
}

OVERALL_RESULT=0

Should_succeed_if_all_tests_from_dir_mapped_as_volume_succeed
RES=$?; if [ "$RES" != "0" ]; then OVERALL_RESULT=1; fi

Should_fail_if_one_test_from_dir_mapped_as_volume_fails
RES=$?; if [ "$RES" != "0" ]; then OVERALL_RESULT=1; fi

exit $OVERALL_RESULT
