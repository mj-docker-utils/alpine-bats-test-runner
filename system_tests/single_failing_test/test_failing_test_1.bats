#!/usr/bin/env bats

@test "Failing test case 1" {

    # GIVEN

    # WHEN
    run echo "Failure."

    # THEN
	[ "$status" -eq 0 ]
    [ "$output" = "Success." ]
}
