#!/usr/bin/env bats

@test "Successful test case 2" {

    # GIVEN

    # WHEN
    run echo "Success."

    # THEN
	[ "$status" -eq 0 ]
    [ "$output" = "Success." ]
}
