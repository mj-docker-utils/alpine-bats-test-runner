#!/usr/bin/env bats

load /usr/local/bin/bats-support.bash
load /usr/local/bin/bats-assert.bash

@test "assert works" {

    # GIVEN

    # WHEN
    run touch /tmp/file.log 

    # THEN
	assert [ -e /tmp/file.log ]
}

@test "assert_success works" {

    # GIVEN

    # WHEN
    run true

    # THEN
	assert_success
}

@test "assert_failure works" {

    # GIVEN

    # WHEN
    run false

    # THEN
	assert_failure
}

@test "assert_output works" {

    # GIVEN

    # WHEN
    run echo "Test output"

    # THEN
	assert_output "Test output"
}

@test "assert_line works" {

    # GIVEN

    # WHEN
    run echo -e "Test output\nSecond line\nThird line"

    # THEN
	assert_line "Second line"
}

