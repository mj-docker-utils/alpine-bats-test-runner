FROM alpine:latest

# Install basic tools
RUN apk add --update bash git && rm -rf /var/cache/apk/*

# Install the Bats, Bats-Support & Bats-Assert
RUN mkdir -p /usr/local/bats                                                             \
 && git clone --depth 1 https://github.com/sstephenson/bats /tmp/bats                    \
 && cd /tmp/bats && ./install.sh "/usr/local/" && cd /tmp && rm -rf /tmp/bats            \
 && git clone https://github.com/ztombol/bats-support /usr/local/bats/bats-support       \
 && git clone https://github.com/jasonkarns/bats-assert-1 /usr/local/bats/bats-assert

# Deploy the tests running script and grant execution rights
COPY run_all_tests.sh /run_all_tests.sh
COPY bats-support.bash /usr/local/bin/bats-support.bash
COPY bats-assert.bash /usr/local/bin/bats-assert.bash
RUN chmod +x /run_all_tests.sh /usr/local/bin/bats-support.bash /usr/local/bin/bats-assert.bash

# Initialize and run the cron daemon on the container startup
CMD /run_all_tests.sh
